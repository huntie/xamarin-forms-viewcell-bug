# Xamarin Forms `ViewCell` performance bug

Project to demonstrate heavy performance degradation experienced on iOS when changing heights of `ViewCell` elements within a `ListView`.

Reported on Bugzilla as [#60211](https://bugzilla.xamarin.com/show_bug.cgi?id=60211)

## Visual

![Example screen recording](./screenshots/example.gif)
