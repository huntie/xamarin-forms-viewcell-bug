﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace ViewCellPerf
{
    public class ListViewPage : ContentPage
    {
        public ListViewPage()
        {
            var random = new Random();
            List<Item> items = Enumerable.Range(0, 26)
                .Select(x => (char) ((char) x + 'A'))
                .Select(x => new Item {
                    Title = x.ToString(),
                    Detail = string.Join("", Enumerable.Repeat("Lorem ipsum\n", random.Next(1, 5)))
                })
                .ToList();
        
            ListView listView = new ListView {
                ItemsSource = items,
                ItemTemplate = new DataTemplate(() => {
                    ItemCell itemCell = new ItemCell();
                    itemCell.SetBinding(ItemCell.ItemProperty, "Value");

                    return itemCell;
                }),
                HasUnevenRows = true
            };
            listView.ItemTapped += ItemTapped;

            Content = new StackLayout {
                Children = { listView }
            };
        }
        
        void ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var item = e.Item as Item;

            item.IsExpanded ^= true;
        }
    }
    
    public class ItemCell : ViewCell
    {
        public static readonly BindableProperty ItemProperty =
            BindableProperty.Create("Item", typeof(Item), typeof(ItemCell), default(Item));

        public Item Item
        {
            get { return GetValue(ItemProperty) as Item; }
            set { SetValue(ItemProperty, value); }
        }
        
        public ItemCell()
        {
            Label titleLabel = new Label();
            titleLabel.FontSize = 15;
            titleLabel.SetBinding(Label.TextProperty, "Title");

            Label detailLabel = new Label();
            detailLabel.SetBinding(Label.TextProperty, "Detail");
            detailLabel.SetBinding(Label.IsVisibleProperty, "IsExpanded");

            View = new StackLayout {
                Padding = new Thickness(20, 15),
                Spacing = 10,
                Children = {
                    titleLabel,
                    detailLabel
                }
            };

            detailLabel.PropertyChanged += (sender, e) => {
                if (e.PropertyName == "IsVisible")
                    ForceUpdateSize();
            };
        }
    }
    
    public class Item : ObservableObject
    {
        string title;
        string detail;
        bool isExpanded;
    
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }
    
        public string Detail
        {
            get { return detail; }
            set { SetProperty(ref detail, value); }
        }
    
        public bool IsExpanded
        {
            get { return isExpanded; }
            set { SetProperty(ref isExpanded, value); }
        }
    }
}
