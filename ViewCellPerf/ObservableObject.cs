﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ViewCellPerf
{
    public class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Set the value for a property, calling <c>OnPropertyChanged</c> if updated.
        /// </summary>
        /// <param name="field">Reference to the field used for storage.</param>
        /// <param name="value">The value to set.</param>
        /// <param name="propertyName">The property name, inferred from the calling context.</param>
        /// <returns><c>true</c>, if updated, <c>false</c> otherwise.</returns>
        protected bool SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (Equals(field, value))
                return false;

            field = value;
            OnPropertyChanged(propertyName);

            return true;
        }
    }
}
